package es.almata.dam.string.exercici001;

import java.util.Scanner;

public class Exercici002 {

	public static void main(String[] args) {
		
		boolean iguals=false;
		int intents=0;
		final String parauladPas="monaito";
		
		Scanner password = new Scanner(System.in);
		String sText = "";
		
		System.out.println("Introdueix la paraula de pas: ");
		sText=password.nextLine();

		
		do {  // do while --> per a que ho pregunti al menys 1 cop
			password.nextLine();
			System.out.println(" Torna a introduir la paraula de pas: ");
			sText=password.nextLine();
			
			for (int i = 0; i <= sText.length(); i++) { // for --> per a que recorri tot el text
				if (sText.equals(parauladPas)) {
					System.out.println("Felicitats!!! l'has endevinat");
					iguals = true;
					break;
				} else {
					System.out.println("Llàstima!!! no l'has endevinat");
					intents++;
					break;
				}
			}
			System.out.println("Torna-ho a probar");
		} while (intents<3);
		if(!iguals)System.out.println("\n - Llàstima!!! no l'has endevinat");
		
		password.close();
	}
}
