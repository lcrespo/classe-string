package es.almata.dam.string.exercici001;

import java.util.Scanner;

public class Exercici001 {

	public static void main(String[] args) {
		
		/*Scanner in=new Scanner(System.in);
		String nombre="";
		System.out.println("Cual es tu nombre?");
		nombre=in.nextLine();
		*/
		
		Scanner lectura = new Scanner(System.in);
		String sText = "";
		System.out.println("Introdueix un text: ");
		sText=lectura.nextLine();
		
		int contador=0;
		for(int i=0;i<sText.length();i++) { 
			if(sText.charAt(i)=='e') {
				contador++;
			}
		}
		System.out.println("El text conte " + contador + " d''e' ");
		lectura.close();
	}

}
