package es.almata.dam.string.exercici001;

import java.util.Scanner;

public class Exercici006 {

	public static void main(String[] args) {
		
		Scanner home=new Scanner(System.in);
		String sText="";
		String paraula="home";
		int nVegadesH=0;
		System.out.println("Introdueix un text");
		sText=home.nextLine();
		if(sText==paraula) {
			nVegadesH++;
		}
		System.out.println(sText);
		System.out.println(paraula);
		System.out.println("La paraula 'home' apareix " + nVegadesH + " vegades");
		home.close();
	}

}
