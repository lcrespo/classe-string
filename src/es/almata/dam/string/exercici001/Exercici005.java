package es.almata.dam.string.exercici001;

import java.util.Scanner;

public class Exercici005 {
	
	public static void main(String[] args) {
		
		Scanner consonants=new Scanner(System.in);
		String sText="";
		System.out.println("Introdueix un text");
		sText=consonants.nextLine();
		
		int nCons=0;
		for (int i = 0; i < sText.length(); i++) {
			if (sText.charAt(i) != 'a' && 
				sText.charAt(i) != 'e' && 
				sText.charAt(i) != 'i' && 
				sText.charAt(i) != 'o' && 
				sText.charAt(i) != 'u' && 
				sText.charAt(i) != ' ')
				nCons++;
		}
		System.out.println("Aqeust text té "+ nCons + " consonants");
		consonants.close();
	}
	
}
