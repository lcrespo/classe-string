package es.almata.dam.string.exercici001;

import java.util.Scanner;

public class Exercici004 {
	public static void main(String[] args) {
		
		Scanner vocals=new Scanner(System.in);
		String sText="";
		sText=vocals.nextLine();
		System.out.println("Introdueix un text");
		sText=vocals.nextLine();
		
		int nVocals=0;
		for (int i = 0; i < sText.length(); i++) {
			if (sText.charAt(i) == 'a' || 
				sText.charAt(i) == 'e' || 
				sText.charAt(i) == 'i' || 
				sText.charAt(i) == 'o'|| 
				sText.charAt(i) == 'u')
				nVocals++;
		}
		System.out.println("El text té "+ nVocals + " vocals");
		vocals.close();
	}
}
