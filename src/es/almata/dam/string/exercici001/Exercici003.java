package es.almata.dam.string.exercici001;

import java.util.Scanner;

public class Exercici003 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner lectura = new Scanner(System.in);
		String sText = "";
		System.out.println("Introdueix un text: ");
		sText=lectura.nextLine();
		
		int nCar=sText.length();
		System.out.println("El text té " + nCar + " caràcters");
		lectura.close();
	}

}
